import './App.css';
import {useDispatch, useSelector} from "react-redux";
import {useState} from "react";
//import {changeUsersName} from "./redux/actions/changeUsersName";
//import {changeUsersAge} from "./redux/actions/changeUsersAge";
import {changeuserage, changeusername} from "./redux/userSlice";

function App() {
    const user = useSelector((state) => state.user);
    console.log(user.name)
    const dispatch = useDispatch();

    const [name, setName] = useState('');
    const [age, setAge] = useState('');
    console.log(age)
    return (
        <div className="App">
            <h1>Name: {user.name}</h1>
            <h1>Age: {user.age}</h1>
            <form
                onSubmit={(e) => {
                    e.preventDefault(); //prevent page refresh
                    dispatch(changeusername(name));
                    dispatch(changeuserage(age));
                    setName('');
                    setAge('')
                }}
            >
                <input
                    onChange={(e) => setName(e.target.value)}
                    value={name}
                    placeholder="Change name"
                />
                <input
                    onChange={(e) => setAge(e.target.value)}
                    value={age}
                    placeholder="Change age"
                />
                <input type="submit" value="Change user details"/>
            </form>
        </div>
    );
}

export default App;
