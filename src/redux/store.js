// # PLAIN REDUX WITH CREATESTORE

// import {createStore} from "redux";
// import allReducers from './reducers';
// const store = createStore(
//     allReducers,
//     window.__REDUX_DEVTOOLS_EXTENSION__&& window.__REDUX_DEVTOOLS_EXTENSION__()
// );
//
// store.getState();
// console.log(store.getState())
// export default store;


// #REDUX WITH RTK --configurestore

import {configureStore} from "@reduxjs/toolkit";
import userReducer from './userSlice';

export const store = configureStore({
    reducer: {
        user: userReducer,
    },
});