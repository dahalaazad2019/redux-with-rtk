import {createSlice} from "@reduxjs/toolkit";

const initialState = {name: 'User 1', age: 26};

export const userSlice = createSlice({
    name: 'user',
    initialState,
    reducers: {
        changeusername: (state, action) => {
            return {...state, name: action.payload};

        },
        changeuserage: (state, action) => {
            return {...state, age: action.payload};
        },
    }
});

export const {changeusername,changeuserage} = userSlice.actions;

export default userSlice.reducer;